# Cloud Computing

## MainActivity

Mal llamado *"Main"*, implementa el tutorial adjunto en el enunciado del laboratorio (agrega una cita en firebase, y obtiene la misma).

## InspiringQuote

Clase que representa una cita. Contiene los atributos quote (frase), author (autor) y points (puntos).

## QuotesActivity

Actividad que contiene el listado de las citas persistidas en Firebase. El mismo se va actualizando a medida que se detectan cambios en la nube.

Contiene un botón, el cual abre un dialogo para crear una nueva cita.

También permite puntuar cada cita, con un rango de 0 a 5 estrellas.

## NewQuoteDialog

Dialogo que persiste una nueva cita en la nube.