package com.soft.arza.ars_2019;

import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.j2objc.annotations.ObjectiveCName;
import com.soft.arza.ars_2019.model.InspiringQuote;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private static final String AUTHOR_KEY = "author";
    private static final String QUOTE_KEY = "quote";
    private View view;
    private TextView quoteTextView;
    private DocumentReference documentReference = FirebaseFirestore.getInstance().document("sampleData/inspiration");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        View submit = findViewById(R.id.button);
        quoteTextView = findViewById(R.id.textView);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view = v;
                saveQuote();
            }
        });
        View fetch = findViewById(R.id.button2);
        fetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view = v;
                fetchQuote();
            }
        });
    }

    public void fetchQuote(){
        documentReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                InspiringQuote quote = documentSnapshot.toObject(InspiringQuote.class);
                quoteTextView.setText("\"" + quote.getQuote() + "\" -- " + quote.getAuthor());
            }
        });
    }

    public void saveQuote(){
        EditText quoteView = findViewById(R.id.editTextQuote);
        EditText authorView = findViewById(R.id.editTextAuthor);
        Map<String,Object> dataToSave = new HashMap<String,Object>();
        dataToSave.put(QUOTE_KEY,quoteView.getText().toString());
        dataToSave.put(AUTHOR_KEY,authorView.getText().toString());
        documentReference.set(dataToSave).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Log.i("saveQuote","Quote was saved");
                    Snackbar.make(view,"Quoted was saved", Snackbar.LENGTH_SHORT).show();
                }else{
                    Log.e("saveQuote","Quote wasn't saved");
                    Snackbar.make(view,"Quote wasn't saved", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }
}
