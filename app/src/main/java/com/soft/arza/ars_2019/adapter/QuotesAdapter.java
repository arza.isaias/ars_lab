package com.soft.arza.ars_2019.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.soft.arza.ars_2019.R;
import com.soft.arza.ars_2019.model.InspiringQuote;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class QuotesAdapter extends ArrayAdapter<InspiringQuote> implements View.OnClickListener{
    private ArrayList<InspiringQuote> quotes;
    private InspiringQuote current;
    private Context mContext;
    private static final String QUOTES_KEY = "inspiringQuotes";
    private DocumentReference documentReference = FirebaseFirestore.getInstance().document("ars/quotes");

    public QuotesAdapter(ArrayList<InspiringQuote> quotes, Context context) {
        super(context, R.layout.quote_item, quotes);
        this.quotes = quotes;
        this.mContext=context;

    }

    public QuotesAdapter(Context context) {
        super(context, R.layout.quote_item);
        this.quotes = new ArrayList<InspiringQuote>();
        this.mContext=context;

    }

    public ArrayList<InspiringQuote> getQuotes() {
        return quotes;
    }

    public void setQuotes(ArrayList<InspiringQuote> quotes) {
        this.quotes = quotes;
    }

    @Override
    public void onClick(View v) {

    }

    public void refreshQuotes(ArrayList<InspiringQuote> refreshedQuotes){
        this.quotes.clear();
        this.quotes.addAll(refreshedQuotes);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.quote_item,parent,false);

        InspiringQuote currentQuote = quotes.get(position);

        final TextView quote = (TextView) listItem.findViewById(R.id.view_quote);
        quote.setText("\"" +currentQuote.getQuote() +"\"");

        final TextView author = (TextView) listItem.findViewById(R.id.view_author);
        author.setText(currentQuote.getAuthor());


        final RatingBar points = listItem.findViewById(R.id.view_point);
        points.setMax(5);
        points.setRating(currentQuote.getPoints());
        points.setNumStars(5);
        points.setStepSize(1);
        points.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                long newPoints = Math.round(rating);
                current = new InspiringQuote(quote.getText().toString().replace("\"", ""),author.getText().toString(),newPoints);
                Log.i("current",current.toString());
                documentReference.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        ArrayList<HashMap> hashMaps = (ArrayList<HashMap>) documentSnapshot.get(QUOTES_KEY);
                        ArrayList<InspiringQuote> quotes = new ArrayList<InspiringQuote>();

                        if(hashMaps != null) {
                            for (HashMap hashMap : hashMaps) {
                                quotes.add(new InspiringQuote(hashMap.get("quote").toString(), hashMap.get("author").toString(), (Long) hashMap.get("points")));
                            }
                            for(InspiringQuote inspiringQuote: quotes){
                                if(inspiringQuote.equals(current)){
                                    inspiringQuote.setPoints(current.getPoints());
                                    Log.i("newPoints",inspiringQuote.toString());
                                }
                            }
                            Map<String,Object> dataToSave = new HashMap<String,Object>();
                            dataToSave.put(QUOTES_KEY,quotes);
                            documentReference.set(dataToSave);
                        }
                    }
                });
            }
        });
        return listItem;
    }
}
