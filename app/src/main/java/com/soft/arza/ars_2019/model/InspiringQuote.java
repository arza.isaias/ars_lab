package com.soft.arza.ars_2019.model;

import android.support.annotation.NonNull;

import java.util.Objects;

public class InspiringQuote {
    private String author;
    private String quote;
    private long points;
    public InspiringQuote(){

    }
    public InspiringQuote(String quote, String author){
        this.author = author;
        this.quote = quote;
    }
    public InspiringQuote(String quote, String author, long points){
        this.author = author;
        this.quote = quote;
        this.points = points;
    }
    public String getAuthor() {
        return author;
    }

    public String getQuote() {
        return quote;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }

    @NonNull
    @Override
    public String toString() {
        return "quote="+this.getQuote() + " author=" + this.getAuthor() + " points=" + this.getPoints();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InspiringQuote that = (InspiringQuote) o;
        return Objects.equals(author, that.author) &&
                Objects.equals(quote, that.quote);
    }

    @Override
    public int hashCode() {
        return Objects.hash(author, quote);
    }
}
