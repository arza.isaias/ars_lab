package com.soft.arza.ars_2019;


import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.soft.arza.ars_2019.adapter.QuotesAdapter;
import com.soft.arza.ars_2019.model.InspiringQuote;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuotesActivity extends AppCompatActivity implements NewQuoteDialog.NewQuoteDialogListener {
    private static final String QUOTES_KEY = "inspiringQuotes";
    private DocumentReference documentReference = FirebaseFirestore.getInstance().document("ars/quotes");
    private List<InspiringQuote> inspiringQuoteList;
    private EditText newQuote;
    private EditText newAuthor;
    ArrayList<InspiringQuote> updatedQuotes;
    ListView listView;
    private static QuotesAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quotes);
        listView = findViewById(R.id.quotes_list_view);
        adapter = new QuotesAdapter(new ArrayList<InspiringQuote>(),getApplicationContext());
        listView.setAdapter(adapter);
        FloatingActionButton addQuoteButton = findViewById(R.id.addQuoteButton);
        documentReference.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable DocumentSnapshot documentSnapshot, @javax.annotation.Nullable FirebaseFirestoreException e) {
                updatedQuotes = new ArrayList<InspiringQuote>();
                ArrayList<HashMap> hashMaps = (ArrayList<HashMap>) documentSnapshot.get(QUOTES_KEY);
                if(hashMaps != null){
                    for(HashMap hashMap: hashMaps){
                        String quote = hashMap.get("quote").toString();
                        String author = hashMap.get("author").toString();
                        int points = Integer.valueOf(hashMap.get("points").toString());
                        Log.i("points",String.valueOf(points));
                        updatedQuotes.add(new InspiringQuote(quote,author,points));
                    }
                }else{
                    updatedQuotes = new ArrayList<InspiringQuote>();
                }

                Log.i("updatedQuotes",updatedQuotes.toString());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.refreshQuotes(updatedQuotes);
                    }
                });
            }
        });
        addQuoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                Fragment prev = fm.findFragmentByTag("dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                DialogFragment dialogFragment = new NewQuoteDialog();
                dialogFragment.show(ft,"newQuoteDialog");
            }
        });
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        newQuote = dialog.getDialog().findViewById(R.id.new_quote);
        newAuthor = dialog.getDialog().findViewById(R.id.new_author);
        documentReference.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                InspiringQuote inspiringQuote = new InspiringQuote(newQuote.getText().toString(),newAuthor.getText().toString());
                if(task.isSuccessful()){
                    Log.i("fetchQuote",task.getResult().toString());
                    inspiringQuoteList = (List<InspiringQuote>) task.getResult().get(QUOTES_KEY);
                    if(inspiringQuoteList == null){
                        Log.i("fetchQuote","Can't fech inspiring quotes");
                        inspiringQuoteList = new ArrayList<InspiringQuote>();
                    }
                    inspiringQuoteList.add(inspiringQuote);
                    Map<String,Object> dataToSave = new HashMap<String,Object>();
                    dataToSave.put(QUOTES_KEY,inspiringQuoteList);
                    documentReference.set(dataToSave);
                }
            }
        });
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }
}
